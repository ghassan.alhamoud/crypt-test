# crypt-test

```
brew install gpg
brew install git-crypt
```

to generate key : 
```
gpg --gen-key 
```

To add the generayed key
```
git-crypt add-gpg-user "Ghassan Alhamoud <ghassan.alhamoud@sunshinesmile.de>"
git-crypt export-key ../crypt-keys
```

move to the repo and run 

```
git-crypt init
```

create the file and the add the files you want to encrypt
```
.gitattributes
````




we may use the same key, but it should be stored in a safe place , 

assume the key is stored in /.keys/

then use the following commands to lock, unlock the ecrypted files 

```
git-crypt unlock /.keys/
git-crypt lock   /.keys/
```


to edit the exsiting key 

```
gpg --edit-key ghassan.alhamoud@sunshinesmile.de
```


some useful commands : 

```
gpg --list-keys
git-crypt status
```


references 
- **[git crypt samples ](https://github.com/quickbooks2018/aws/blob/master/git-crypt)**
- **[git crypt doc](https://github.com/AGWA/git-crypt)**

